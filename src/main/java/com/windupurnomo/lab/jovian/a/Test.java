/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.windupurnomo.lab.jovian.a;

/**
 *
 * @author windupurnomo
 */
public class Test {
    public static void main(String[] args) {
        //Answer for question A-1 instance of employee class
        Employee employee = new Employee();
        Employee employee1 = new Employee("windu purnomo", 3000);
        
        
        //Answer for question A-2 print numbers
        //Note: I can't get subject-point of this number.
        //  in this context we print numbers that multiple 100. Buth 2nd statement state that 'but not a multiple of 100'
        //  I write a code that evaluate number that multiple 400
        for (int i = 0; i < 10000; i+=100) {
            String s = i % 400 == 0 ? i + " (leap year)" : String.valueOf(i);
            System.out.println(s);
        }
    }
}
