/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.windupurnomo.lab.jovian.a;

/**
 *
 * @author windupurnomo
 */
public class Employee {
    private String name;
    private double salary;

    /**
     * Default constructor
     */
    public Employee() {
    }

    /**
     * Constructor with parameters name and salary
     * @param name      Employee name
     * @param salary    Employee salary
     */
    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }
    
    
    
    //setters and getters method
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
    
}
