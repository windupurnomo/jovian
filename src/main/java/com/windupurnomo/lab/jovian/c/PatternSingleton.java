/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.windupurnomo.lab.jovian.c;

/**
 *
 * @author windupurnomo
 */
public class PatternSingleton {

    //create an object of SingleObject
    private static PatternSingleton instance = new PatternSingleton();

   //make the constructor private so that this class cannot be
    //instantiated
    private PatternSingleton() {
    }

    //Get the only object available
    public static PatternSingleton getInstance() {
        return instance;
    }

    public void showMessage() {
        System.out.println("Hello World!");
    }
}
