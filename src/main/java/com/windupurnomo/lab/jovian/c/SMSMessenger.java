/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.windupurnomo.lab.jovian.c;

/**
 * Implementation of sending message using SMS media
 * @author windupurnomo
 */
public class SMSMessenger implements IMessenger{
    
    @Override
    public void send(String content) {
        System.out.println("Send message using SMS: "+ content);
    }
    
}
