package com.windupurnomo.lab.jovian.c.factoryPatt;

/**
 *
 * @author windupurnomo
 */
public class Square implements Shape {

    @Override
    public void draw() {
        System.out.println("Inside Square::draw() method.");
    }
}
