package com.windupurnomo.lab.jovian.c.factoryPatt;

/**
 *
 * @author windupurnomo
 */
public interface Shape {
    void draw();
}
