/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.windupurnomo.lab.jovian.c;

/**
 *
 * @author windupurnomo
 */
public interface IMessenger {
    void send(String content);
}
