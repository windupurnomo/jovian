package com.windupurnomo.lab.jovian.c;

/**
 *
 * @author windupurnomo
 */
public class PatternSingletonDemo {
    public static void main(String[] args) {
        //illegal construct
        //Compile Time Error: The constructor SingleObject() is not visible
        //SingleObject object = new SingleObject();
        //Get the only object available
        PatternSingleton object = PatternSingleton.getInstance();

        //show the message
        object.showMessage();
    }
}
