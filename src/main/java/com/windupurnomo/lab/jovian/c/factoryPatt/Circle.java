package com.windupurnomo.lab.jovian.c.factoryPatt;

/**
 *
 * @author windupurnomo
 */
public class Circle implements Shape {

    @Override
    public void draw() {
        System.out.println("Inside Circle::draw() method.");
    }
}
