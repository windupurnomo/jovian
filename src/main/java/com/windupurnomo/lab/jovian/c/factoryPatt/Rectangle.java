package com.windupurnomo.lab.jovian.c.factoryPatt;

/**
 *
 * @author windupurnomo
 */
public class Rectangle implements Shape {
    @Override
    public void draw() {
        System.out.println("Inside Rectangle::draw() method.");
    }
}
