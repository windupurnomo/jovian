package com.windupurnomo.lab.jovian.c;

/**
 * It is polymorpishm
 * @author windupurnomo
 */
public class Test {
    private static int env = 0;
    public static void main(String[] args) {
        IMessenger im;
        switch(env){
            case 0: im = new SMSMessenger();
            case 1: im = new EmailMessenger();
            default:im = new SMSMessenger();
        }
        im.send("Hello world");
    }
}
