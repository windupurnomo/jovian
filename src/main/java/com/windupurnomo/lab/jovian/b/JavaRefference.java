/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.windupurnomo.lab.jovian.b;

/**
 *
 * @author windupurnomo
 */
public class JavaRefference {
    public static void main(String[] args) {
        new JavaRefference().test();
    }
    
    private void test(){
        IntHolder x = new IntHolder();
        IntHolder y = new IntHolder();
        x.value = 1;
        y.value = 5;
        new JavaRefference().swap(x, y);
        System.out.println("x : " + x.value);
        System.out.println("y : " + y.value);
    }
    
    
    
    
    //java, pass by value not refference
    class IntHolder {
        public int value = 0;
    }

    private void swap(IntHolder a, IntHolder b) {
    // Although a and b are copies, they are copies *of a reference*.
        // That means they point at the same object as in the caller,
        // and changes made to the object will be visible in both places.
        int temp = a.value;
        a.value = b.value;
        b.value = temp;
    }
    
}


/* 
Another languages: C
We can use pointer passing by reference to swap

public void Swap(int &p, int &q)
{
   int temp;
   temp = *p;
   *p = *q;
   *q = temp;
} 

*/

