/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.windupurnomo.lab.jovian.b;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author windupurnomo
 */
public class SundaysFile {
    private static final int YEAR = 2015;
    
    public static void main(String[] args) {
        try {
            File file = new File("/home/windupurnomo/sandbox/sundays.txt");
            
            // if file doesnt exists, then create it
            if (!file.exists()) 
                file.createNewFile();

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(getSundays());
            bw.close();

            System.out.println("Done");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private static String getSundays (){
        StringBuilder sb = new StringBuilder();
        //This constructor is deprecated, so I don't use this one
        //Date d = new Date(2015, 1, 1);
        
        Calendar cal = new GregorianCalendar(YEAR, Calendar.JANUARY, 1);
        int inc = 1;
        for (int i = 0; i < 366 && cal.get(Calendar.YEAR) == YEAR; i += inc) {
            if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                // this is a sunday
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                sb.append(format.format(cal.getTime())).append("\n");
                cal.add(Calendar.DAY_OF_MONTH, 7);
                inc = 7;
            } else {
                //increment by one until we get first sunday in a year
                cal.add(Calendar.DAY_OF_MONTH, 1);
            }
        }
        return sb.toString();
    }
}
