/*
To easier maintain data 
I use declaration and inisialitation variable approach
*/

DO $$

DECLARE emp1 uuid;
DECLARE emp2 uuid;
DECLARE emp3 uuid;
DECLARE emp4 uuid;
DECLARE emp5 uuid;

DECLARE addr1 uuid;
DECLARE addr2 uuid;
DECLARE addr3 uuid;
DECLARE addr4 uuid;
DECLARE addr5 uuid;

BEGIN


emp1 := uuid_generate_v1();
emp2 := uuid_generate_v1();
emp3 := uuid_generate_v1();
emp4 := uuid_generate_v1();
emp5 := uuid_generate_v1();


addr1 := uuid_generate_v1();
addr2 := uuid_generate_v1();
addr3 := uuid_generate_v1();
addr4 := uuid_generate_v1();
addr5 := uuid_generate_v1();


-- create employee table
create table employee(
	id uuid primary key,
	name character varying (100),
	salary double precision
);

-- create address table
CREATE TABLE address
(
  id uuid primary key,
  employee_id uuid,
  address character varying(150),
  CONSTRAINT employee_address_fk FOREIGN KEY (employee_id)
      REFERENCES employee (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);


-- create employee data
insert into employee (id, name, salary) values
(emp1, 'Windu Purnomo', 15000),
(emp2, 'James Bond', 3000),
(emp3, 'Clark Kent', 7000),
(emp4, 'Tony Stark', 12000),
(emp5, 'Peter Parker', 1800);


-- create address data 
insert into address (id, employee_id, address) values
(addr1, emp1, 'Bogor'),
(addr2, emp2, 'Banten'),
(addr3, emp3, 'Batak');


-- data dummy to show difference between left and right join
insert into address (id, address) values
(addr4, 'Bogor'),
(addr5, 'Batak');


END $$;
