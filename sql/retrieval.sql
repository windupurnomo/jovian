--Retrieve the Name of the Employee with the highest Salary
select name from employee order by salary desc offset 0 limit 1;


--Retrieve the Name of the Employee with the second highest Salary
select name from employee order by salary desc offset 1 limit 1;

-- left join will make all employee data (left side) will be shown, even though address data (right side) is null
select e.name, a.address from employee e left join address a on e.id = a.employee_id;

-- inner join will shown the existing data on both sides and right
select e.name, a.address from employee e inner join address a on e.id = a.employee_id;

-- right join will make all address data (right side) will be shown, even though employee data (left side) is null
select e.name, a.address from employee e right join address a on e.id = a.employee_id;

-- Employees in High Sallary group
select * from employee where salary > 10000;

-- Employees in Med Sallary group
select * from employee where salary between 2000 and 10000;

-- Employees in High Sallary group
select * from employee where salary < 2000;